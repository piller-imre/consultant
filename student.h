#ifndef STUDENT_H
#define STUDENT_H

#include <QList>
#include <QString>

/**
 * @brief Checks that the needle is in the ordered subset of the haystack.
 * @param needle the searched expression
 * @param haystack the text which maybe contains the expression
 * @return true, when the needle is in the haystack, else false
 */
bool isOrderedSubset(const QString& needle, const QString& haystack);

/**
 * @brief Load the data of the student from a simple text file.
 * @param path the path of the student list file
 * @return list of the student Neptun codes and names
 */
QList<QString> loadStudentData(const QString& path);

/**
 * @brief Find students in the list of students.
 * @param query query expression for searching
 * @param students list of students
 * @return list of matching students
 */
QList<QString> findStudents(const QString& query, const QList<QString>& students);

/**
 * @brief Get the Neptun code from the text value.
 * @param text Neptun code and name
 * @return the Neptun code in upper case
 */
QString getNeptunCode(const QString& text);

/**
 * @brief Collect the filenames of the given student.
 * @param neptun Neptun code
 * @return list of available files
 */
QList<QString> collectFileNames(const QString& neptun);

/**
 * @brief Create the missing student directories.
 * @param students list of student Neptun codes and names
 */
void createMissingDirectories(const QList<QString>& students);

#endif // STUDENT_H
