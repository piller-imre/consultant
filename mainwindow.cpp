#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "student.h"

#include <QDir>
#include <QProcess>
#include <QStringList>

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    qDebug() << "Current directory: " << QDir::currentPath();

    _students = loadStudentData("students.txt");
    createMissingDirectories(_students);
    ui->studentListView->setModel(&_resultModel);
    ui->studentDirectoryView->setModel(&_filesModel);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::queryChanged(const QString &query)
{
    saveText();
    QList<QString> results = findStudents(query, _students);
    QStringList strings(results);
    _resultModel.setStringList(strings);
    QStringList emptyList;
    _filesModel.setStringList(emptyList);
}

void MainWindow::selectStudent(const QModelIndex& index)
{
    saveText();
    QString value = index.data().toString();
    QString neptun = getNeptunCode(value);
    _neptun = neptun;
    qDebug() << "selected ..." << neptun;
    QList<QString> studentFiles = collectFileNames(neptun);
    QStringList fileNames(studentFiles);
    _filesModel.setStringList(fileNames);
}

void MainWindow::selectFile(const QModelIndex& index)
{
    saveText();
    QString value = index.data().toString();
    _fileName = value;
    qDebug() << "File: " << value;
    QString path("data/");
    path += _neptun;
    path += "/";
    path += _fileName;
    QFile file(path);
    if (file.open(QIODevice::ReadOnly)) {
        QTextStream stream(&file);
        QString content = stream.readAll();
        ui->plainTextEdit->document()->setPlainText(content);
        file.close();
    }
    else {
        throw std::runtime_error("Unable to open the selected file!");
    }
}

void MainWindow::saveText()
{
    if (_fileName.isEmpty() == false) {
        QString content = ui->plainTextEdit->document()->toPlainText();
        qDebug() << "Save ..";
        qDebug() << content;
        QString path("data/");
        path += _neptun;
        path += "/";
        path += _fileName;
        QFile file(path);
        if (file.open(QIODevice::WriteOnly)) {
            QTextStream stream(&file);
            stream << content;
            file.close();
        }
        else {
            throw std::runtime_error("Unable to save the selected file!");
        }
        _fileName.clear();
        ui->plainTextEdit->document()->setPlainText(":: Saved! ::");
    }
}

void MainWindow::createFile()
{
    QString fileName = ui->lineEdit->text();
    qDebug() << "Create file:" << fileName;
    if (_neptun.isEmpty() == false && fileName.isEmpty() == false) {
        saveText();
        _fileName = fileName;
        ui->plainTextEdit->document()->setPlainText("");
        QString path("data/");
        path += _neptun;
        path += "/";
        path += _fileName;
        QFile file(path);
        if (file.open(QIODevice::WriteOnly)) {
            file.close();
            QList<QString> studentFiles = collectFileNames(_neptun);
            QStringList fileNames(studentFiles);
            _filesModel.setStringList(fileNames);
        }
        else {
            throw std::runtime_error("Unable to save the selected file!");
        }
    }
}

void MainWindow::openDirectory()
{
    QString command = QString("pcmanfm data/%1").arg(_neptun);
    QProcess* process = new QProcess();
    process->start(command);
}
