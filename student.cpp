#include "student.h"

#include <exception>

#include <QDir>
#include <QFile>
#include <QTextStream>

#include <QDebug>

bool isOrderedSubset(const QString& needle, const QString& haystack)
{
    if (needle.isEmpty()) {
        return true;
    }
    if (haystack.isEmpty()) {
        return false;
    }
    if (needle.toLower() == haystack.toLower()) {
        return true;
    }
    if (needle.size() > haystack.size()) {
        return false;
    }

    int a = 0;
    int b = needle.size() - 1;
    int i = -1;
    int j = haystack.size();

    while (a <= b) {
        while (true) {
            ++i;
            if (i == haystack.size()) {
                return false;
            }
            if (haystack[i].toLower() == needle[a].toLower()) {
                break;
            }
        }
        while (true) {
            --j;
            if (j == -1) {
                return false;
            }
            if (haystack[j].toLower() == needle[b].toLower()) {
                break;
            }
        }
        ++a;
        --b;
    }
    if (i < j) {
        return true;
    }
    else if (i > j) {
        return false;
    }
    else {
        return a - b == 2;
    }
}

QList<QString> loadStudentData(const QString& path)
{
    QList<QString> students;
    QFile studentFile(path);
    if (studentFile.open(QIODevice::ReadOnly)) {
        QTextStream stream(&studentFile);
        while (stream.atEnd() == false) {
            QString line = stream.readLine();
            students.append(line);
        }
        studentFile.close();
    }
    else {
        throw std::runtime_error("Unable to open the student file!");
    }
    return students;
}

QList<QString> findStudents(const QString& query, const QList<QString>& students)
{
    QList<QString> results;
    for (const QString& student : students) {
        if (isOrderedSubset(query, student)) {
            results.append(student);
        }
    }
    return results;
}

QString getNeptunCode(const QString& text)
{
    if (text.length() < 6) {
        throw std::runtime_error("The text is too short for containing the Neptun code!");
    }
    return text.left(6).toUpper();
}

QList<QString> collectFileNames(const QString& neptun)
{
    QString baseDir = "data/";
    QString studentPath = baseDir + neptun;
    QDir directory = QDir(studentPath);
    if (directory.exists() == false) {
        throw std::runtime_error("The directory of the user does not exists!");
    }
    QList<QString> fileNames;
    for (const QString& fileName : directory.entryList(QDir::Filter::Files)) {
        // if (fileName != "." && fileName != "..") {
            fileNames.append(fileName);
        // }
    }
    return fileNames;
}

void createMissingDirectories(const QList<QString>& students)
{
    for (const QString& student : students) {
        QString neptun = getNeptunCode(student);
        QString path("data/");
        path += neptun;
        if (QDir(path).exists() == false) {
            QDir().mkdir(path);
        }
    }
}
