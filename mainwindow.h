#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QList>
#include <QMainWindow>
#include <QString>
#include <QStringListModel>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void queryChanged(const QString& query);
    void selectStudent(const QModelIndex& index);
    void selectFile(const QModelIndex& index);
    void createFile();
    void openDirectory();

private:
    Ui::MainWindow *ui;

    QList<QString> _students;
    QStringListModel _resultModel;
    QList<QString> _studentFiles;
    QStringListModel _filesModel;

    QString _neptun;
    QString _fileName;

    void saveText();
};

#endif // MAINWINDOW_H
